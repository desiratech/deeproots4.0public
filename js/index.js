webpackJsonp([0],{

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	var _reactDom = __webpack_require__(32);

	var _configureStore = __webpack_require__(179);

	var _configureStore2 = _interopRequireDefault(_configureStore);

	var _rootReducer = __webpack_require__(203);

	var _rootReducer2 = _interopRequireDefault(_rootReducer);

	var _Root = __webpack_require__(211);

	var _Root2 = _interopRequireDefault(_Root);

	var _BaseLayout = __webpack_require__(229);

	var _BaseLayout2 = _interopRequireDefault(_BaseLayout);

	__webpack_require__(232);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	//Reducers


	//Components


	//Base CSS


	var initialState = {
	    footer: "Poo",
	    navigation: {
	        currentPage: "home",
	        items: [{
	            name: "Home",
	            url: "home"
	        }, {
	            name: "Classes",
	            url: "class"
	        }, {
	            name: "Gallery",
	            url: "gal"
	        }]
	    }
	};

	var store = (0, _configureStore2.default)(_rootReducer2.default, initialState);

	var BoilerPlate = function (_Component) {
	    _inherits(BoilerPlate, _Component);

	    function BoilerPlate() {
	        _classCallCheck(this, BoilerPlate);

	        return _possibleConstructorReturn(this, (BoilerPlate.__proto__ || Object.getPrototypeOf(BoilerPlate)).apply(this, arguments));
	    }

	    _createClass(BoilerPlate, [{
	        key: 'render',
	        value: function render() {
	            return _react2.default.createElement(_BaseLayout2.default, store);
	        }
	    }]);

	    return BoilerPlate;
	}(_react.Component);

	(0, _reactDom.render)(_react2.default.createElement(
	    _Root2.default,
	    { store: store },
	    _react2.default.createElement(BoilerPlate, null)
		), document.getElementById('app'));

/***/ },

/***/ 179:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.default = configureStore;

	var _redux = __webpack_require__(180);

	var _reduxDevtoolsExtension = __webpack_require__(201);

	var _reduxThunk = __webpack_require__(202);

	var _reduxThunk2 = _interopRequireDefault(_reduxThunk);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function configureStore(rootReducer, initialState) {
	    return (0, _redux.createStore)(rootReducer, initialState, getEnhancer());
	}

	function getEnhancer() {
	    return (0, _redux.compose)((0, _redux.applyMiddleware)(_reduxThunk2.default));
	}

/***/ },

/***/ 201:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(window) {"use strict";

	var compose = __webpack_require__(180).compose;

	exports.__esModule = true;
	exports.composeWithDevTools = (
	  typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
	    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ :
	    function() {
	      if (arguments.length === 0) return undefined;
	      if (typeof arguments[0] === 'object') return compose;
	      return compose.apply(null, arguments);
	    }
	);

	exports.devToolsEnhancer = (
	  typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION__ ?
	    window.__REDUX_DEVTOOLS_EXTENSION__ :
	    function() { return function(noop) { return noop; } }
	);

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(34)))

/***/ },

/***/ 203:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _redux = __webpack_require__(180);

	var _genericActions = __webpack_require__(204);

	var _lodash = __webpack_require__(205);

	var _lodash2 = _interopRequireDefault(_lodash);

	var _immutable = __webpack_require__(206);

	var _immutable2 = _interopRequireDefault(_immutable);

	var _NavigationReducer = __webpack_require__(207);

	var _NavigationReducer2 = _interopRequireDefault(_NavigationReducer);

	var _FooterReducer = __webpack_require__(209);

	var _FooterReducer2 = _interopRequireDefault(_FooterReducer);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	//reducers
	var initialState = {
	    global: {
	        currentPage: 'home'
	    },
	    errors: {}
	};

	function errors() {
	    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState.errors;
	    var action = arguments[1];

	    switch (action.type) {
	        default:
	            break;
	    }
	    return state;
	}

	function global() {
	    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState.global;
	    var action = arguments[1];

	    switch (action.type) {
	        default:
	            break;
	    }
	    return state;
	}

	var rootReducer = (0, _redux.combineReducers)({
	    global: global,
	    errors: errors,
	    navigation: _NavigationReducer2.default,
	    footer: _FooterReducer2.default
	});

		exports.default = rootReducer;

/***/ },

/***/ 204:
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var genericActionTypes = exports.genericActionTypes = {
	    "PAGE_LOADED": "PAGE_LOADED",
	    "RETRIEVE_SITE_DATA": "RETRIEVE_SITE_DATA",
	    "ERROR_STATE": "ERROR_STATE"
		};

/***/ },

/***/ 207:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _NavigationActions = __webpack_require__(208);

	var _immutable = __webpack_require__(206);

	var _immutable2 = _interopRequireDefault(_immutable);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var initialState = {
	    navigation: {
	        items: []
	    }
	};

	function navigation() {
	    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState.navigation;
	    var action = arguments[1];

	    switch (action.type) {
	        case _NavigationActions.NavigationActionTypes.CLICK_NAV:
	            return _immutable2.default.fromJS(state).mergeDeep({ currentPage: action.pageUrl }).toJS();
	        default:
	            return state;
	    }

	    return state;
	}

	exports.default = navigation;

/***/ },

/***/ 208:
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.handleNavigationChange = handleNavigationChange;
	var NavigationActionTypes = exports.NavigationActionTypes = {
	    "CLICK_NAV": "CLICK_NAV"
	};

	function handleNavigationChange(pageUrl) {
	    return {
	        type: NavigationActionTypes.CLICK_NAV,
	        pageUrl: pageUrl
	    };
		}

/***/ },

/***/ 209:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _FooterActions = __webpack_require__(210);

	var _immutable = __webpack_require__(206);

	var _immutable2 = _interopRequireDefault(_immutable);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var initialState = {
	    footer: {
	        items: []
	    }
	};

	function footer() {
	    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState.footer;
	    var action = arguments[1];

	    switch (action.type) {
	        case _FooterActions.FooterActionTypes.CLICK_FOOTER_NAV:
	            //Do something;
	            return state;
	        default:
	            return state;
	    }

	    return state;
	}

	exports.default = footer;

/***/ },

/***/ 210:
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var FooterActionTypes = exports.FooterActionTypes = {
	    "CLICK_FOOTER_NAV": "CLICK_FOOTER_NAV"
		};

/***/ },

/***/ 211:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	var _reactRedux = __webpack_require__(212);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var Root = function (_Component) {
	    _inherits(Root, _Component);

	    function Root() {
	        _classCallCheck(this, Root);

	        return _possibleConstructorReturn(this, (Root.__proto__ || Object.getPrototypeOf(Root)).apply(this, arguments));
	    }

	    _createClass(Root, [{
	        key: 'render',
	        value: function render() {
	            return _react2.default.createElement(
	                _reactRedux.Provider,
	                { store: this.props.store },
	                _react2.default.createElement(
	                    'div',
	                    null,
	                    this.props.children
	                )
	            );
	        }
	    }]);

	    return Root;
	}(_react.Component);

		exports.default = Root;

/***/ },

/***/ 229:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	var _reactDom = __webpack_require__(32);

	var _reactRedux = __webpack_require__(212);

	var _Navigation = __webpack_require__(230);

	var _Navigation2 = _interopRequireDefault(_Navigation);

	var _Footer = __webpack_require__(231);

	var _Footer2 = _interopRequireDefault(_Footer);

	var _index = __webpack_require__(242);

	var _index2 = _interopRequireDefault(_index);

	var _index3 = __webpack_require__(243);

	var _index4 = _interopRequireDefault(_index3);

	var _index5 = __webpack_require__(244);

	var _index6 = _interopRequireDefault(_index5);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	//BaseLayout


	//Custom Pages


	var BaseLayout = function (_Component) {
	    _inherits(BaseLayout, _Component);

	    function BaseLayout() {
	        _classCallCheck(this, BaseLayout);

	        return _possibleConstructorReturn(this, (BaseLayout.__proto__ || Object.getPrototypeOf(BaseLayout)).apply(this, arguments));
	    }

	    _createClass(BaseLayout, [{
	        key: 'componentDidMount',
	        value: function componentDidMount() {}
	    }, {
	        key: 'render',
	        value: function render() {

	            return _react2.default.createElement(
	                'div',
	                { className: 'base-container' },
	                _react2.default.createElement(_Navigation2.default, null),
	                this.renderContent(),
	                this.renderFooter()
	            );
	        }
	    }, {
	        key: 'renderFooter',
	        value: function renderFooter() {
	            return typeof this.props.footer !== 'undefined' ? _react2.default.createElement(_Footer2.default, { content: this.props.footer }) : null;
	        }
	    }, {
	        key: 'renderContent',
	        value: function renderContent() {
	            var navigation = this.props.navigation;

	            switch (navigation.currentPage) {
	                case 'home':
	                    return _react2.default.createElement(_index2.default, null);
	                case 'class':
	                    return _react2.default.createElement(_index6.default, null);
	                case 'gal':
	                    return _react2.default.createElement(_index4.default, null);
	            }
	        }
	    }]);

	    return BaseLayout;
	}(_react.Component);

	function mapStateToProps(state) {
	    return {
	        global: state.global,
	        errors: state.errors,
	        navigation: state.navigation,
	        footer: state.footer
	    };
	}
	exports.default = (0, _reactRedux.connect)(mapStateToProps)(BaseLayout);

/***/ },

/***/ 230:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	var _reactDom = __webpack_require__(32);

	var _reactRedux = __webpack_require__(212);

	var _lodash = __webpack_require__(205);

	var _lodash2 = _interopRequireDefault(_lodash);

	var _NavigationActions = __webpack_require__(208);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	//Actions


	var Navigation = function (_Component) {
	    _inherits(Navigation, _Component);

	    function Navigation() {
	        _classCallCheck(this, Navigation);

	        return _possibleConstructorReturn(this, (Navigation.__proto__ || Object.getPrototypeOf(Navigation)).apply(this, arguments));
	    }

	    _createClass(Navigation, [{
	        key: 'render',
	        value: function render() {
	            var _this2 = this;

	            var nav = this.props.nav;

	            console.log(nav.currentPage);
	            return _react2.default.createElement(
	                'nav',
	                { className: 'nav' },
	                _react2.default.createElement(
	                    'ul',
	                    null,
	                    _lodash2.default.map(nav.items, function (navItem) {
	                        return _react2.default.createElement(
	                            'li',
	                            { key: Math.random() + "-nav-item" },
	                            _react2.default.createElement(
	                                'a',
	                                { className: 'nav-item', onClick: _this2.onNavClick.bind(_this2, navItem.url) },
	                                navItem.name
	                            )
	                        );
	                    })
	                )
	            );
	        }
	    }, {
	        key: 'onNavClick',
	        value: function onNavClick(url) {
	            var dispatch = this.props.dispatch;

	            dispatch((0, _NavigationActions.handleNavigationChange)(url));
	        }
	    }]);

	    return Navigation;
	}(_react.Component);

	function mapStateToProps(state) {
	    return {
	        nav: state.navigation
	    };
	}

	exports.default = (0, _reactRedux.connect)(mapStateToProps)(Navigation);

/***/ },

/***/ 231:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	var _reactDom = __webpack_require__(32);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var Footer = function (_Component) {
	    _inherits(Footer, _Component);

	    function Footer() {
	        _classCallCheck(this, Footer);

	        return _possibleConstructorReturn(this, (Footer.__proto__ || Object.getPrototypeOf(Footer)).apply(this, arguments));
	    }

	    _createClass(Footer, [{
	        key: 'componentDidMount',
	        value: function componentDidMount() {}
	    }, {
	        key: 'render',
	        value: function render() {
	            console.log('footer', this.props);
	            return _react2.default.createElement(
	                'footer',
	                { className: 'footer' },
	                this.props.content
	            );
	        }
	    }]);

	    return Footer;
	}(_react.Component);

		exports.default = Footer;

/***/ },

/***/ 232:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },

/***/ 242:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	var _reactDom = __webpack_require__(32);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var HomePage = function (_Component) {
	    _inherits(HomePage, _Component);

	    function HomePage() {
	        _classCallCheck(this, HomePage);

	        return _possibleConstructorReturn(this, (HomePage.__proto__ || Object.getPrototypeOf(HomePage)).apply(this, arguments));
	    }

	    _createClass(HomePage, [{
	        key: 'componentDidMount',
	        value: function componentDidMount() {}
	    }, {
	        key: 'render',
	        value: function render() {
	            return _react2.default.createElement(
	                'div',
	                null,
	                'HomeMufu'
	            );
	        }
	    }]);

	    return HomePage;
	}(_react.Component);

		exports.default = HomePage;

/***/ },

/***/ 243:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	var _reactDom = __webpack_require__(32);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var Gallery = function (_Component) {
	    _inherits(Gallery, _Component);

	    function Gallery() {
	        _classCallCheck(this, Gallery);

	        return _possibleConstructorReturn(this, (Gallery.__proto__ || Object.getPrototypeOf(Gallery)).apply(this, arguments));
	    }

	    _createClass(Gallery, [{
	        key: 'componentDidMount',
	        value: function componentDidMount() {}
	    }, {
	        key: 'render',
	        value: function render() {
	            return _react2.default.createElement(
	                'div',
	                null,
	                'GalleryMufu'
	            );
	        }
	    }]);

	    return Gallery;
	}(_react.Component);

		exports.default = Gallery;

/***/ },

/***/ 244:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	var _reactDom = __webpack_require__(32);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var Classes = function (_Component) {
	    _inherits(Classes, _Component);

	    function Classes() {
	        _classCallCheck(this, Classes);

	        return _possibleConstructorReturn(this, (Classes.__proto__ || Object.getPrototypeOf(Classes)).apply(this, arguments));
	    }

	    _createClass(Classes, [{
	        key: 'componentDidMount',
	        value: function componentDidMount() {}
	    }, {
	        key: 'render',
	        value: function render() {
	            return _react2.default.createElement(
	                'div',
	                null,
	                'ClassesMufu'
	            );
	        }
	    }]);

	    return Classes;
	}(_react.Component);

		exports.default = Classes;

/***/ }

});
//# sourceMappingURL=index.js.map